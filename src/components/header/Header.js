import React, { useCallback, useState } from 'react';
import {Link} from 'react-router-dom';
import './header.css';
import ChangeHistoryIcon from '@mui/icons-material/ChangeHistory';
import {useNavigate} from 'react-router-dom';

export default function Header(props) {
  let navigate = useNavigate(); 
  const [name, setName] = useState(localStorage.getItem("name"))
  const [toggle, setToggle] = useState(true);
  const setProfileAndToggle = useCallback(() => {
    navigate(`/profile/${name}`, { replace: true })
  });
  return (
    <header className='header'>
        <div className='header-container'>
            <div className='title'>Cinta Coding</div>
            {(props?.page === 'home' || props?.page === 'detail') && <div className='posts-tab'><div>Posts</div></div>}
            { ((props?.page === 'home' && name) || (props?.page === 'detail' && name)) ? <div className='profile' onClick={() => setToggle(!toggle)}> {!toggle && <div className='sub-menu' onClick={() => setProfileAndToggle()}><span className='icon-triangle'><ChangeHistoryIcon/></span><span className='text-menu'>Detail Profile</span></div>}<div className='welcome-text'>Welcome, <span className='name'>{name}</span></div></div> : <Link to="/login">
                <button className='button-login'>Login</button>
            </Link> }
        </div>
    </header>
  )
}
