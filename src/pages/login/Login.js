import {useCallback, useEffect, useState} from 'react';
import './login.css';
import { useNavigate } from "react-router-dom";
import Alert from '@mui/material/Alert';

export default function Login() {
  const [users, setUsers] = useState([]);
  let navigate = useNavigate(); 
  const [username, setUsername] = useState(0);
  const [wrongUsername, setWrongUsername] = useState(false);
  
  const fetchUsers = useCallback(async() => {
    const users = await fetch("https://jsonplaceholder.typicode.com/users");
    const returnUsers = await users.json();
    setUsers(returnUsers);
  });

  useEffect(() => {
    fetchUsers();
  }, [wrongUsername]);

  const loginUser = useCallback(() => {
    if (username) {
      for (let val of users) {
        if (val['username'] === username) {
          localStorage.setItem("name", val['name'].split(' ')[0]);
          localStorage.setItem("username", val['name']);
          localStorage.setItem("email", val['email']);
          localStorage.setItem("phone", val['phone']);
          localStorage.setItem("address", val['address']['street']);
          navigate(`/home`, { replace: true })
        } else {
          console.log('cant enter the home');
          setTimeout(() => {
            setWrongUsername(false);
          }, 2000);
          setWrongUsername(true);
        }
      }
    }
  });
  return (
    <div className='login-page'>
        <div className='login-page-container'>
            <div className='title'>Login Page</div>
            <div className='form'>
                {wrongUsername && <Alert variant="filled" severity="error" className='wrong-alert'>
                 Wrong username!
                </Alert>}
                <input placeholder='username' type='text' className='username' required onChange={(e) => setUsername(e.target.value)}/>
                <input placeholder='password' className='password' type='password' required/>
                <button className='button-login' onClick={loginUser}>Login</button>
            </div>
        </div>
    </div>
  )
}
